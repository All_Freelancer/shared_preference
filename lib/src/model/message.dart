
//****************************** CLASS MESSAGIN ********************************
class MessageNotification {
  String title;
  String body;
  //String location;

  //******************************* CONSTRUCT **********************************
  MessageNotification();

  //************************** CONVERT TO JSON *********************************
  MessageNotification.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        body = json['body'];

  //************************** CONVERT TO JSON *********************************
  Map<String, dynamic> toJson() => {
    'title': title,
    'body': body,
  };
}
//******************************************************************************
