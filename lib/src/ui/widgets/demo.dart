import 'package:flutter/material.dart';
import 'package:shared_preference_firebase/src/data/shared_pref.dart';
import 'package:shared_preference_firebase/src/model/message.dart';

//******************************** CLASS DEMO **********************************
class Demo extends StatefulWidget {

  //**********************
  @override
  DemoView createState() {
    return DemoView();
  }
}

//********************************* STATE DEMO *********************************
class DemoView extends State<Demo> {
  SharedPref sharedPref = SharedPref();
  MessageNotification notificationSave = MessageNotification();
  MessageNotification notificationLoad = MessageNotification();

  //**************************** METHOD LOAD SHARED PREF ***********************
  loadSharedPrefs() async {

    try {
      MessageNotification user = MessageNotification.fromJson(await sharedPref.read("message"));

      //:::::::::::::::::: SNACK BAR ::::::::::::::::::
      Scaffold.of(context).showSnackBar(SnackBar(
          content: new Text("Loaded!"),
          duration: const Duration(milliseconds: 500)));

      //:::::::::::::::: SET STATE :::::::
      setState(() {
        notificationLoad = user;
      });

    } catch (Excepetion) {

      //:::::::::::::::::: SNACK BAR ::::::::::::::::::
      Scaffold.of(context).showSnackBar(SnackBar(
          content: new Text("Nothing found!"),
          duration: const Duration(milliseconds: 500)));
    }
  }

  //***************************** ROOT WIDGET DEMO *****************************
  @override
  Widget build(BuildContext context) {

    //::::::::::::::::::::: RETURN LIST VIEW ::::::::::::::::::::::::
    return ListView(
      children: <Widget>[
        //:::::::::::::::::::::::::: CONTAINER :::::::::::
        Container(
          height: 200.0,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                  height: 50.0,
                  width: 300.0,
                  child: TextField(
                    decoration: InputDecoration(hintText: "Tilte"),
                    onChanged: (value) {
                      setState(() {
                        notificationSave.title = value;
                      });
                    },
                  )),
              Container(
                  height: 50.0,
                  width: 300.0,
                  child: TextField(
                    decoration: InputDecoration(hintText: "Body"),
                    onChanged: (value) {
                      setState(() {
                        notificationSave.body = value;
                      });
                    },
                  )),

              Text("Array-> "),
            ],
          ),
        ),

        //:::::::::::::::::::::::::::::::: 2º CONTAINER ::::::::::::::::::::::::
        Container(
          height: 80.0,

          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,

            children: <Widget>[

              RaisedButton(
                onPressed: () {
                  sharedPref.save("message", notificationSave);

                  Scaffold.of(context).showSnackBar(SnackBar(
                      content: new Text("Saved!"),
                      duration: const Duration(milliseconds: 500)));
                },

                child: Text('Save', style: TextStyle(fontSize: 20)),
              ),


              RaisedButton(
                onPressed: () {
                  loadSharedPrefs();
                },
                child: Text('Load', style: TextStyle(fontSize: 20)),
              ),


              RaisedButton(
                onPressed: () {
                  sharedPref.remove("message");
                  Scaffold.of(context).showSnackBar(SnackBar(
                      content: new Text("Cleared!"),
                      duration: const Duration(milliseconds: 500)));
                  setState(() {
                    notificationLoad = MessageNotification();
                  });
                },
                child: Text('Clear', style: TextStyle(fontSize: 20)),
              ),
            ],
          ),
        ),

        //:::::::::::::::::::::::::::::::::: SIZE BOX :::::::::::::::::::::::::
        SizedBox(
          height: 300.0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,

            children: <Widget>[
              Text("title: " + (notificationLoad.title ?? ""),
                  style: TextStyle(fontSize: 16)),
              Text("body: " + (notificationLoad.body ?? ""),
                  style: TextStyle(fontSize: 16)),

            ],
          ),
        ),
      ],
    );
  }
}
//******************************************************************************
