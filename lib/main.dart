import 'package:flutter/material.dart';
import 'package:shared_preference_firebase/src/ui/pages/home.dart';

//****************************** CLASS MAIN ************************************
///void main() => runApp(MyApp());

void main() async {
  //await DatabaseCreator().initDatabase();
  runApp(MyApp());
}
//****************************** CLASS ROOT ************************************
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Save Message in SharedPreferences'),
    );
  }
}


//******************************************************************************
